package com.supermap.baselibrary.base;

import android.content.Context;

/**
 * Created by Administrator on 2017/6/27 0027.
 */

public interface IBaseView {
    void success(String msg);

    void error(String msg);

    void fail(String msg);

    IBasePresenter getPresenter();

    void removePresenter();

    void loading();

    void dismissLoading();

    Context getContext();
}
