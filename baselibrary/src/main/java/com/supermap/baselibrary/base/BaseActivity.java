package com.supermap.baselibrary.base;

import android.content.Context;
import android.widget.EditText;

import com.google.gson.Gson;
import com.supermap.baselibrary.utils.NetworkUtils;
import com.supermap.baselibrary.utils.ProgressUtils;
import com.supermap.baselibrary.utils.ToastUtil;

public abstract class BaseActivity extends BaseContext {
    private Gson gson;
    private ProgressUtils progressUtils = new ProgressUtils(getContext());

    @Override
    public Context getContext() {
        return this;
    }

    public Gson getGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!NetworkUtils.isAvailable(this)) {
            showToast("网络不可用，请连接网络");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!NetworkUtils.isAvailable(this)) {
            showToast("网络不可用，请连接网络");
        }
    }


    /**
     * 获取ed里面的数据
     *
     * @param editText
     * @return
     */
    public String getEdContent(EditText editText) {
        return editText != null ? editText.getText().toString() : "";
    }

    /**
     * 用来显示msg的toast
     *
     * @param msg
     */
    protected void showToast(String msg) {
        ToastUtil.showToast(getApplicationContext(), msg);
    }

    /**
     * 用来显示msg的toast
     *
     * @param msg
     */
    protected void showLongToast(String msg) {
        ToastUtil.showLongToast(getApplicationContext(), msg);
    }

    @Override
    public void loading() {
        progressUtils.showProgressDialog();
    }

    @Override
    public void dismissLoading() {
        progressUtils.cancelProgressDialog();
    }

    @Override
    public void success(String msg) {
        showToast(msg);
    }

    @Override
    public void error(String msg) {
        showToast(msg);
    }

    @Override
    public void fail(String msg) {
        showToast(msg);
    }
}
