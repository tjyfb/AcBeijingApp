package com.supermap.baselibrary.base;

import com.google.gson.Gson;

public abstract class BasePresenter {
    protected IBaseView iBaseView;
    protected static Gson gson = new Gson();

    protected BasePresenter(IBaseView iBaseView) {
        this.iBaseView = iBaseView;
        if (gson == null) {
            gson = new Gson();
        }
    }

    public static Gson getGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }
}
