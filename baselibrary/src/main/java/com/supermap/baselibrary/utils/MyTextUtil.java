package com.supermap.baselibrary.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Administrator on 2017/4/5 0005.
 */

public class MyTextUtil {
    /**
     * 给textView设置内容
     *
     * @param textView
     * @param content
     */
    public static void setContent(TextView textView, String content) {
        if (textView == null || content == null) {
            return;
        }
        if (TextUtils.isEmpty(content.trim())) {
            textView.setText("");
        } else {
            textView.setText(content);
        }
    }

    /**
     * 判断字符串是否为空字符串
     *
     * @param context
     * @param content
     * @return
     */
    public static boolean checkEmpty(Context context, String... content) {
        for (String msg : content) {
            if (TextUtils.isEmpty(msg)) {
                ToastUtil.showToast(context, "请输入完整");
                return false;
            }
            //
        }
        return true;
    }

    /*
    *  2017/9/21  -  10:33
    *  获取ed的内容
    */
    public static String getEditContent(EditText editText) {
        String content = "";
        if (editText != null) {
            content = editText.getText().toString().trim().replace(" ", "").trim();
            return content;
        } else {
            return content;
        }
    }
}
