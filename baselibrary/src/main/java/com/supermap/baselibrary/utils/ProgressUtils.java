package com.supermap.baselibrary.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

public class ProgressUtils {
    private final Context context;
    private static ProgressUtils progressUtils;

    public ProgressUtils(Context context) {
        this.context = context;
    }

    /**
     * 加载的进度条
     *
     * @param ev
     * @return
     */
    protected ProgressDialog mAlertDialog;

    /**
     * 四个重载的显示进度条的方法
     *
     * @return
     */
    public ProgressDialog showProgressDialog() {
        return showProgressDialog("", "请稍后", null);
    }

    protected ProgressDialog showProgressDialog(String pTitle, String pMessage) {
        return showProgressDialog(pTitle, pMessage, null);
    }

    protected ProgressDialog showProgressDialog(String pTitle) {
        return showProgressDialog(pTitle, "请稍后", null);
    }

    protected synchronized ProgressDialog showProgressDialog(String pTitle, String pMessage,
                                                             DialogInterface.OnCancelListener pCancelClickListener) {
        if (mAlertDialog != null) {
            mAlertDialog.setTitle(pTitle);
            mAlertDialog.setMessage(pMessage);
            return mAlertDialog;
        } else {
            try {
                mAlertDialog = ProgressDialog.show(context, pTitle, pMessage, true, true);
                mAlertDialog.setCancelable(true);
                mAlertDialog.setCanceledOnTouchOutside(false);
                mAlertDialog.setOnCancelListener(pCancelClickListener);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return (ProgressDialog) mAlertDialog;
    }

    /**
     * 取消进度条
     */
    public synchronized void cancelProgressDialog() {
        if (mAlertDialog != null) {
            mAlertDialog.cancel();
            mAlertDialog.dismiss();
//            if (mAlertDialog.isShowing()) {
//                mAlertDialog.dismiss();
//                mAlertDialog.cancel();
//            }
            mAlertDialog = null;
        }
    }
}
