package com.supermap.baselibrary.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

/**
 * Created by Administrator on 2017/3/23 0023.
 */

/**
 * 不会一直弹的toast工具类
 */
public class ToastUtil {
    private static Toast mToast = null;

    public static void showToast(Context context, String msg) {
        if (mToast != null) {
            mToast.cancel();
        }
        if (!TextUtils.isEmpty(msg)) {
            mToast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
            mToast.show();
        }
    }
    public static void showLongToast(Context context, String msg) {
        if (mToast != null) {
            mToast.cancel();
        }
        if (!TextUtils.isEmpty(msg)) {
            mToast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
            mToast.show();
        }
    }
}
