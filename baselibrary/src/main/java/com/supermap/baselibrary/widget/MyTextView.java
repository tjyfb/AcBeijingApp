package com.supermap.baselibrary.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Administrator on 2017/3/30 0030.
 */

public class MyTextView extends TextView {

    public MyTextView(Context context) {
        super(context);
        initTv();
    }


    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTv();
    }

    public MyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initTv();
    }

    private void initTv() {
        this.setTextColor(Color.BLACK);  //此处定义颜色
    }
}
