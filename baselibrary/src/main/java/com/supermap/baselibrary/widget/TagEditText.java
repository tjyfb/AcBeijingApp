package com.supermap.baselibrary.widget;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.supermap.baselibrary.R;

/**
 * Created by Administrator on 2017/7/7 0007.
 */

public class TagEditText extends LinearLayout {
    private Context context;
    private TextView tagTextView;
    private EditText editText;
    private String tagTextStr = "superMap";
    private String hintText = "";
    private int editTextBgColor;//输入框背景颜色
    private int tagTextColor;//tag字符串的字体颜色
    private float shapeRoundRadius;//圆角半径大小
    private int tagTextWidth;//宽度
    private int screenWidth;
    private boolean editAble;
    private int inputType;//输入类型

    public TagEditText(Context context) {
        super(context);
        init(context, null);
    }

    public TagEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }


    public TagEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        this.context = context;
        screenWidth = getDeviceWidth(context);
        this.setOrientation(HORIZONTAL);
        /**
         * 得到自定义属性的值
         */
        getAttrsValue(context, attrs);
        /**
         *开始添加tagText
         */
        addTagText();
        /**
         * 开始添加editText—
         */
        addEditText();
        /**
         * —设置shape
         */
        setShapeToEditText();
    }

    /**
     * 获取屏幕宽度(返回的是px)
     *
     * @param context
     * @return
     */
    public static int getDeviceWidth(Context context) {
        DisplayMetrics outMetrics = obtain(context);
        return outMetrics.widthPixels;
    }

    /**
     * 获取DisplayMetrics
     *
     * @param context
     * @return
     */
    private static DisplayMetrics obtain(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        return dm;
    }

    private void addTagText() {
        /**
         * 添加tv和img
         */
        tagTextView = new TextView(context);
        tagTextView.setText(tagTextStr);
        LayoutParams tagTextViewLayoutParams = new LayoutParams(tagTextWidth, LayoutParams
                .WRAP_CONTENT);
        tagTextViewLayoutParams.gravity = Gravity.CENTER;
        tagTextViewLayoutParams.setMargins(dp2px(context, 10), dp2px(context, 5), dp2px(context, 10), dp2px(context,
                5));
        tagTextView.setLayoutParams(tagTextViewLayoutParams);
        tagTextView.setTextColor(tagTextColor);
        addView(tagTextView);
    }

    private void addEditText() {
        initEditText();
        //设置输入类型
        setInputType();
        addView(editText);
    }

    private void initEditText() {
        editText = new EditText(context);
        setEdLayoutParams();
        //是否可编辑
        editText.setEnabled(editAble);
    }

    private void setEdLayoutParams() {
        LayoutParams editTextLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
        editTextLayoutParams.setMargins(dp2px(context, 10), dp2px(context, 5), dp2px(context, 10), dp2px(context, 5));
        editTextLayoutParams.gravity = Gravity.CENTER;
        editText.setLayoutParams(editTextLayoutParams);
        editText.setPadding(dp2px(context, 10), dp2px(context, 10), dp2px(context, 10), dp2px(context, 10));
    }

    private void setInputType() {
        if (inputType == 0) {//other--text
            editText.setInputType(InputType.TYPE_CLASS_TEXT);
        } else if (inputType == 1) {//phone
            editText.setInputType(InputType.TYPE_CLASS_PHONE);
        } else if (inputType == 2) {//number
            editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        } else if (inputType == 3) {//bigNumber
            editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        }
    }

    private void setShapeToEditText() {
        int strokeWidth = 5; // 3px not dp
        float roundRadius = shapeRoundRadius; // 8px not dp
        int strokeColor = Color.parseColor("#2E3135");
//        int fillColor = Color.parseColor("#DFDFE0");
        GradientDrawable gd = new GradientDrawable();
        gd.setColor(editTextBgColor);
        gd.setCornerRadius(roundRadius);
//        gd.setStroke(strokeWidth, strokeColor);
        if (editText.isEnabled()) {
            editText.setBackground(gd);
        } else {
            GradientDrawable disableBg = new GradientDrawable();
            disableBg.setColor(Color.parseColor("#a6a1a1"));
            gd.setCornerRadius(roundRadius);
            editText.setBackground(disableBg);
        }

    }

    /**
     * 获取editText里面的字符串
     *
     * @return
     */
    public String getEditTextContent() {
        String content = "";
        if (editText != null) {
            content = editText.getText().toString();
        }
        return content;
    }

    /**
     * 给editText设置content
     *
     * @param content
     */
    public void setEditTextContent(String content) {
        if (editText != null) {
            if (content == null) {
                content = "";
            }
            editText.setText(content);
        }
    }

    /**
     * 直接获取editText
     *
     * @return
     */
    public EditText getEditText() {
        return editText;
    }

    /**
     * 获取自定义的属性数据
     *
     * @param context
     * @param attrs
     */
    private void getAttrsValue(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TagEditText);
            if (typedArray != null) {
                editAble = typedArray.getBoolean(R.styleable.TagEditText_editable, true);
                tagTextStr = typedArray.getString(R.styleable.TagEditText_tagText) == null ? tagTextStr : typedArray
                        .getString(R.styleable.TagEditText_tagText);
                hintText = typedArray.getString(R.styleable.TagEditText_hintText) == null ? hintText : typedArray
                        .getString(R.styleable.TagEditText_hintText);
                editTextBgColor = typedArray.getColor(R.styleable.TagEditText_editTextBgColor, Color.parseColor
                        ("#FFECECEC"));//背景默认灰色
                tagTextColor = typedArray.getColor(R.styleable.TagEditText_tagTextColor, Color.BLACK);//默认黑色
                shapeRoundRadius = typedArray.getDimension(R.styleable.TagEditText_roundRadius, 0);//默认圆角的大小是0
                tagTextWidth = (int) typedArray.getDimension(R.styleable.TagEditText_tagTextWidth, dp2px(context, 45)
                );//默认宽度是50dp
                inputType = typedArray.getInt(R.styleable.TagEditText_inputType, 0);
                typedArray.recycle();
            }
        }
    }

    /**
     * dp转px
     *
     * @param context
     * @param dpVal
     * @return
     */
    public int dp2px(Context context, float dpVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                dpVal, context.getResources().getDisplayMetrics());
    }

    /**
     * sp转px
     *
     * @param context
     * @param spVal
     * @return
     */
    public int sp2px(Context context, float spVal) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                spVal, context.getResources().getDisplayMetrics());
    }

}
