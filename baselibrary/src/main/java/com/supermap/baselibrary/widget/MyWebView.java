package com.supermap.baselibrary.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.AttributeSet;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.supermap.baselibrary.utils.ProgressUtils;


/**
 * 支持css和js特效
 */
public class MyWebView extends WebView {

    private ProgressUtils progressUtils;

    public MyWebView(Context context) {
        super(context);
        init(this);
        show(context);
    }

    public MyWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(this);
        show(context);
    }

    public MyWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(this);
        show(context);
    }

    private void show(Context context) {
        progressUtils = new ProgressUtils(context);
    }


    private void init(MyWebView webView) {
        //
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        // 设置可以支持缩放
        webSettings.setSupportZoom(true);
        // 设置出现缩放工具
        webSettings.setBuiltInZoomControls(false);
        //扩大比例的缩放
        webSettings.setUseWideViewPort(true);
        //自适应屏幕
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);//WebSettings.LOAD_CACHE_ELSE_NETWORK使用缓存
        /**
         * WebView加载不出Html5网页的解决方法
         */
        webSettings.setDomStorageEnabled(true);
        /**
         *提高渲染的优先级
         */
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
//        WebSettings webSettings = getSettings();
        // User settings
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        /**
         *
         */
        webSettings.setTextSize(WebSettings.TextSize.NORMAL);
        /**
         *
         */
//        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setDisplayZoomControls(false);
        webSettings.setJavaScriptEnabled(true); // 设置支持javascript脚本
        webSettings.setAllowFileAccess(true); // 允许访问文件
//        webSettings.setBuiltInZoomControls(true); // 设置显示缩放按钮
//        webSettings.setSupportZoom(true); // 支持缩放
        webSettings.setLoadsImagesAutomatically(true);//auto load images
//        webView.getSettings().setUseWideViewPort(true); //auto adjust screen
        webSettings.setLoadWithOverviewMode(true);

        /**
         * 把图片加载放在最后来加载渲染
         * Sets whether the WebView should not load image resources from the
         * network
         */
        webSettings.setBlockNetworkImage(false);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new MyWebViewClient());
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            // html加载完成之后，添加监听图片的点击js函数
            progressUtils.cancelProgressDialog();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressUtils.showProgressDialog();
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            // html加载完成之后，添加监听图片的点击js函数
            progressUtils.cancelProgressDialog();
        }
    }
}
