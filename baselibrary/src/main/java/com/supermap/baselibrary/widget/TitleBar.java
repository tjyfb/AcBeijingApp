package com.supermap.baselibrary.widget;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.supermap.baselibrary.R;
import com.supermap.baselibrary.utils.DensityUtils;


public class TitleBar extends RelativeLayout {
    private TextView titleTv;
    private ImageView imageView;

    public TitleBar(Context context) {
        super(context);
        init(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void setTitle(String title) {
        titleTv.setText(title);
        imageView.setVisibility(VISIBLE);
    }

    public void setTitle(String title, boolean show) {
        titleTv.setText(title);
        if (show) {
            imageView.setVisibility(VISIBLE);
        } else {
            imageView.setVisibility(GONE);
        }
    }

    private void init(final Context context) {
        /**
         * 添加tv和img
         */
        titleTv = new TextView(context);
        titleTv.setText("超图软件");
        LayoutParams titleTvLayoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup
                .LayoutParams.WRAP_CONTENT);
        titleTvLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        //
        titleTv.setLayoutParams(titleTvLayoutParams);
        titleTv.setTextColor(getResources().getColor(R.color.title_bar_tv));
        /**
         *  setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
         */
        titleTv.setTextSize(18);
        titleTv.setMaxLines(20);
        //
        imageView = new ImageView(context);
        imageView.setBackgroundResource(R.drawable.rerun_arrow);
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams
                .WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
        layoutParams.setMargins(DensityUtils.dp2px(context, 5), DensityUtils.dp2px(context, 15), DensityUtils.dp2px
                (context, 15), 0);
        imageView.setLayoutParams(layoutParams);

        //
        imageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Activity activity = (Activity) context;
                    if (activity != null) {
                        activity.finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imageView.setVisibility(GONE);
        /**
         * 把view添加到layout
         */
        addView(imageView);
        addView(titleTv);
        /**
         * 设置layout
         */
        this.setBackgroundColor(getResources().getColor(R.color.title_bar_bg));
    }
}
