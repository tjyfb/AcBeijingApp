package com.supermap.AcBeijingApp.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.supermap.AcBeijingApp.R;
import com.supermap.AcBeijingApp.model.IndexChildBean;
import com.supermap.AcBeijingApp.model.IndexParentBean;
import com.supermap.baselibrary.utils.DensityUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bruce on 2017/10/25 -- 17:28
 */
/*
*  2017/10/26  -  9:39
*  指标列表的适配器
*/
public class TopicListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private List<IndexParentBean> arrayList = new ArrayList();

    public TopicListAdapter(Context context) {
        this.context = context;
    }

    public void setData(List arrayList) {
        if (arrayList != null) {
            this.arrayList = arrayList;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getGroupCount() {
        return arrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return arrayList.get(groupPosition).getList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return arrayList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return arrayList.get(groupPosition).getList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.item_expandlist_parent, null, false);
        AbsListView.LayoutParams lp = new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DensityUtils.dp2px(context, 50f));
        relativeLayout.setLayoutParams(lp);
        TextView textView = (TextView) relativeLayout.findViewById(R.id.expand_parent_name);
        IndexParentBean indexParentBean = (IndexParentBean) getGroup(groupPosition);
        textView.setText(indexParentBean.getContent());
        return relativeLayout;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.item_expandlist_child, null, false);
        TextView textView = (TextView) relativeLayout.findViewById(R.id.expand_child_name);
        IndexParentBean indexParentBean = (IndexParentBean) getGroup(groupPosition);
        IndexChildBean indexChildBean = indexParentBean.getList().get(childPosition);
        textView.setText(indexChildBean.getContent());
        return relativeLayout;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return childId;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return groupId;
    }
}
