package com.supermap.AcBeijingApp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.supermap.AcBeijingApp.R;
import com.supermap.AcBeijingApp.model.FocusMsgBean;
import com.supermap.baselibrary.utils.MyTextUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bruce on 2018/3/14 0014.
 */

public class FocusListAdapter extends BaseAdapter {
    private Context context;
    private List arrayList = new ArrayList();

    public FocusListAdapter(Context context) {
        this.context = context;
    }

    public void setData(List arrayList) {
        if (arrayList != null) {
            this.arrayList = arrayList;
            notifyDataSetChanged();
        }
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.focus_item_list, null);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
            viewHolder.description = (TextView) convertView.findViewById(R.id.description);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final FocusMsgBean bean = (FocusMsgBean) getItem(position);
//        Picasso.with(context).load(context.getResources().getDrawable(R.drawable.bg)).into(viewHolder.image);
        MyTextUtil.setContent(viewHolder.description, bean.getTitle());
        return convertView;
    }

    class ViewHolder {
        ImageView image;
        TextView description;
    }
}
