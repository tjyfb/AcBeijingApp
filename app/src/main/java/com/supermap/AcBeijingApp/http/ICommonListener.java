package com.supermap.AcBeijingApp.http;

/**
 * 通用的监听
 */
public interface ICommonListener {
    void result(boolean status, String content);
}
