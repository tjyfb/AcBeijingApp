package com.supermap.AcBeijingApp.http;


import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

public class HttpManger {
    private static HttpManger httpManger;

    public static HttpManger getInstance() {
        if (httpManger == null) {
            synchronized (HttpManger.class) {
                httpManger = new HttpManger();
            }
        }
        return httpManger;
    }

    /*
    *  2017/10/10  -  16:54
    *  简单的get获取数据
    */
    public void getData(String url, final ICommonListener iCommonListener) {
        OkHttpUtils.get().url(url).build().execute(new StringCallback() {
            @Override
            public void onError(okhttp3.Call call, Exception e, int id) {
                iCommonListener.result(false, e.getMessage());
            }

            @Override
            public void onResponse(String response, int id) {
                iCommonListener.result(true, response);
            }
        });
    }

}
