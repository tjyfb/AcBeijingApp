package com.supermap.AcBeijingApp.http;

public class HttpUrl {
    public static String APP_TAG = "&app";
    private static HttpUrl httpUrl;

    private HttpUrl() {
    }

    public static HttpUrl getInstance() {
        if (httpUrl == null) {
            synchronized (HttpUrl.class) {
                httpUrl = new HttpUrl();
            }
        }
        return httpUrl;
    }

    /**
     * 服务器地址
     */
    private String SERVER_HOST;//SERVER +
    private String protocol = "http";
    private String SERVER_IP = SharedPreferencesManager.getUserIP();
    private String SERVER_PORT = SharedPreferencesManager.getUserPort();
    private String PROJECT = "data";

    public String getRequestUrl(final String suffix) {
        return getServerHost() + "/" + this.getPROJECT() + suffix;
    }

    /**
     * 获取请求的host
     *
     * @return
     */
    public String getServetContext() {
        return getServerHost() + "/" + this.getPROJECT();
    }

    public String getSERVER_IP() {
        return SERVER_IP;
    }

    public void setSERVER_IP(String SERVER_IP) {
        this.SERVER_IP = SERVER_IP;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getPROJECT() {
        return PROJECT;
    }

    public void setPROJECT(String PROJECT) {
        this.PROJECT = PROJECT;
    }


    public String getSERVER_PORT() {
        return SERVER_PORT;
    }

    public void setSERVER_PORT(String SERVER_PORT) {
        this.SERVER_PORT = SERVER_PORT;
    }

    public String getServerHost() {
//        String ip = SharedPreferencesManager.getUserIP();
//        String port = SharedPreferencesManager.getUserPort();
//        if (!TextUtils.isEmpty(ip)) {
//            return this.getProtocol() + "://" + ip + ":" + port;
//        }
//        return this.getProtocol() + "://" + this.getSERVER_IP() + ":" + this.getSERVER_PORT();
        return "http://121.42.160.106:8092";
    }

    public void setServerHost(String serverHost) {
        SERVER_HOST = serverHost;
    }

}
