package com.supermap.AcBeijingApp.http;

import android.content.Context;
import android.content.SharedPreferences;

import com.supermap.AcBeijingApp.base.MyApplication;
import com.supermap.AcBeijingApp.utils.Logs;

public class SharedPreferencesManager {
    /**
     * sp 的名字——user
     */
    public final static String USER_INFO = "USER_INFO";
    /**
     * sp 的名字——token
     */
    public final static String TOKEN = "TOKEN";
    /**
     * 字段名字——user是否登录了
     */
    public final static String USER_IS_LOGIN = "USER_IS_LOGIN";
    /**
     * 字段名字——token
     */
    public final static String USER_TOKEN = "USER_TOKEN";
    /**
     * ip
     */
    public final static String USER_IP = "USER_IP";
    /**
     * 字段名字——port
     */
    public final static String USER_PORT = "USER_PORT";
    //是否设置了host
    public final static String SET_HOST = "SET_HOST";
    /**
     * 用户的区划代码-用户的icon
     */
    public final static String REGION_CODE = "REGION_CODE";
    public final static String USER_ICON = "USER_ICON";

    /**
     * 保存用户信息的sp
     *
     * @return
     */
    public static SharedPreferences getUserSharePreferences() {
        return MyApplication.getInstance().getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);
    }

    /**
     * 保存用户token的sp
     *
     * @return
     */
    public static SharedPreferences getUserTokenSp() {
        return MyApplication.getInstance().getSharedPreferences(TOKEN, Context.MODE_PRIVATE);
    }

    /**
     * 保存用户是否登录
     *
     * @param isLogin
     */
    public static void saveUserLogin(boolean isLogin) {
        getUserSharePreferences().edit().putBoolean(USER_IS_LOGIN, isLogin).commit();
    }

    /**
     * 保存用户token
     *
     * @param token
     */
    public static boolean saveUserToken(String token) {
        return getUserTokenSp().edit().putString(USER_TOKEN, token).commit();
    }

    /**
     * 得到用户token
     *
     * @return
     */
    public static String getUserToken() {
        String string = getUserTokenSp().getString(USER_TOKEN, "0");
        Logs.w("得到用户token    " + string);
        return getUserTokenSp().getString(USER_TOKEN, "0");
    }

    /**
     * 清除用户token
     */
    public static void clearUserToken() {
        getUserTokenSp().edit().clear().commit();
        String string = getUserTokenSp().getString(USER_TOKEN, "");
        System.out.println(string);
    }

    /**
     * 清除用户信息
     */
    public static void clearUserInfo() {
        getUserSharePreferences().edit().clear().commit();
    }


    /**
     * 得到用户是否登陆的信息
     *
     * @return
     */
    public static boolean getUserIsLogin() {
        return getUserSharePreferences().getBoolean(USER_IS_LOGIN, false);
    }

    /**
     * 是否设置了host
     *
     * @return
     */
    public static boolean getSetHost() {
        return getUserSharePreferences().getBoolean(SET_HOST, false);
    }

    public static boolean setSetHost(boolean status) {
        return getUserSharePreferences().edit().putBoolean(SET_HOST, status).commit();
    }

    /**
     * 用户的ip和端口设置
     *
     * @param userRegionCode
     */
    public static void setUserIP(String userRegionCode) {
        getUserSharePreferences().edit().putString(USER_IP, userRegionCode).commit();
    }

    public static String getUserIP() {
        return getUserSharePreferences().getString(USER_IP, "");
    }

    public static void setUserPort(String userRegionCode) {
        getUserSharePreferences().edit().putString(USER_PORT, userRegionCode).commit();
    }

    public static String getUserPort() {
        return getUserSharePreferences().getString(USER_PORT, "");
    }

    /*
    *  2017/10/12  -  18:27
    *  用户的区划代码
    */
    public static void setUserRegionCode(String userRegionCode) {
        Logs.w("用户的区划代码" + userRegionCode);
        getUserSharePreferences().edit().putString(REGION_CODE, userRegionCode).commit();
    }

    public static String getUserRegionCode() {
        String regionCode = getUserSharePreferences().getString(REGION_CODE, "");
        Logs.w("用户的区划代码" + regionCode);
        return regionCode;
    }

    /*
       *  2017/10/12  -  18:27
       *  用户的icon
       */
    public static void setUserIcon(String userRegionCode) {
        getUserSharePreferences().edit().putString(USER_ICON, userRegionCode).commit();
    }

    public static String getUserIcon() {
        return getUserSharePreferences().getString(USER_ICON, "");
    }
}
