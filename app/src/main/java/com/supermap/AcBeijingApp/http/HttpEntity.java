package com.supermap.AcBeijingApp.http;

public class HttpEntity {
    // http://127.0.0.1:8080/bjnp/bjapp/login?userName=13123&passWord=123123
    //登录
    public static String LOGIN = "/bjapp/login";
    //http://127.0.0.1:8080/bjnp/bjapp/collectionIndexNews?status=true&indexId=13213123
    //添加或取消到收藏列表
    public static String COLLECTION_INDEX_ADD_CANCEL = "/bjapp/collectionIndexNews";
    //http://127.0.0.1:8080/bjnp/bjapp/getDataMap?userId=1231231
    //获取数据地图
    public static String GET_DATA_MAP = "/bjapp/getDataMap";
    //http://127.0.0.1:8080/bjnp/bjapp/getIndexDetail?indexId=12121
    //获取指标详情数据
    public static String GET_INDEX_DETAIL = "/bjapp/getIndexDetail";
    //http://127.0.0.1:8080/bjnp/bjapp/getMyCollectionList?userId=1212
    //获取我的收藏列表
    public static String GET_COLLECTION_LIST = "/bjapp/getMyCollectionList";
    //http://127.0.0.1:8080/bjnp/bjapp/getNewsDetail?newsId=12121
    //获取新闻详情
    public static String GET_NEWS_DETAIL = "/bjapp/getNewsDetail";
    //http://127.0.0.1:8080/bjnp/bjapp/getRecentNewsList
    //获取最近的新闻--领导关注的新闻
    public static String GET_RECENT_NEWS = "/bjapp/getRecentNewsList";
    //http://127.0.0.1:8080/bjnp/bjapp/getSpecialDetail?specialId=1213
    //获取专题详情
    public static String GET_SPECIAL_DETAIL = "/bjapp/getSpecialDetail";
    //http://127.0.0.1:8080/bjnp/bjapp/searchNews?searchKey=31231241
    //搜索新闻
    public static String SEARCH_NEWS = "/bjapp/searchNews";
    //
    public static String MONTHLY = "/panjin/getMonthlyList";
    public static String MONTH_REPORT = "/panjin/getReportList";

}
