package com.supermap.AcBeijingApp;

import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.supermap.AcBeijingApp.view.fragment.DataFragment;
import com.supermap.AcBeijingApp.view.fragment.FocusFragment;
import com.supermap.AcBeijingApp.view.fragment.HomeFragment;
import com.supermap.AcBeijingApp.view.fragment.PersonFragment;
import com.supermap.AcBeijingApp.view.fragment.TopicFragment;
import com.supermap.baselibrary.base.BaseActivity;
import com.supermap.baselibrary.base.IBasePresenter;

import butterknife.BindView;

/**
 * 主选项卡页面
 *
 * @author bruce
 *         2018/3/14 0014 下午 2:22
 */
public class MainTabActivity extends BaseActivity {

    @BindView(android.R.id.tabhost)
    FragmentTabHost tabHost;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main_tab;
    }

    @Override
    protected void initCommon() {

    }

    @Override
    protected void doBusiness() {
        tabHost.setBackgroundColor(getResources().getColor(R.color.white));
        //使用fragment代替activity转换实现
        tabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        //添加tab页
        addTabs();
    }

    private void addTabs() {
        tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator(setTabMenu("首页", R.drawable.main_home)), HomeFragment
                .class, null);
        tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator(setTabMenu("关注", R.drawable.main_home)), FocusFragment
                .class, null);
        tabHost.addTab(tabHost.newTabSpec("tab3").setIndicator(setTabMenu("数据", R.drawable.main_home)), DataFragment
                .class, null);
        tabHost.addTab(tabHost.newTabSpec("tab4").setIndicator(setTabMenu("专题", R.drawable.main_home)), TopicFragment
                .class, null);
        tabHost.addTab(tabHost.newTabSpec("tab5").setIndicator(setTabMenu("我的", R.drawable.main_home)), PersonFragment
                .class, null);
    }

    //自定义tab
    public View setTabMenu(String name, int image) {
        View view = LayoutInflater.from(MainTabActivity.this).inflate(R.layout.tab_own_item_layout, null);
        view.setBackgroundColor(getResources().getColor(R.color.tab_bg));
        TextView menuText = (TextView) view.findViewById(R.id.tab_menu_txt);
        ImageView menuImg = (ImageView) view.findViewById(R.id.tab_com_img);
        menuText.setText("" + name);
        menuImg.setImageResource(image);
        return view;
    }

    @Override
    public IBasePresenter getPresenter() {
        return null;
    }

}
