package com.supermap.AcBeijingApp.base;


import android.app.Application;

public class MyApplication extends Application {
    public static MyApplication myApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        myApplication = this;
    }
    /**
     * a single instance for application
     *
     * @return
     */
    public static MyApplication getInstance() {
        /**
         * judge application or null
         */
        if (myApplication == null) {
            myApplication = new MyApplication();
        }
        return myApplication;
    }

}
