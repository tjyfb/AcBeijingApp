package com.supermap.AcBeijingApp.view.fragment;


import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.supermap.AcBeijingApp.R;
import com.supermap.AcBeijingApp.adapter.FocusListAdapter;
import com.supermap.AcBeijingApp.model.FocusMsgBean;
import com.supermap.AcBeijingApp.utils.IntentUtils;
import com.supermap.AcBeijingApp.view.activity.CWebViewActivity;
import com.supermap.baselibrary.base.BaseFragment;
import com.supermap.baselibrary.widget.TitleBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 领导关注
 *
 * @author bruce
 * 2018/3/14 0014 下午 2:16
 */
public class FocusFragment extends BaseFragment {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.focus_list_view)
    ListView focusListView;

    public FocusFragment() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_focus;
    }

    @Override
    protected void doBusiness(View rootView) {
        titleBar.setTitle("领导关注", false);
        dealList();
    }

    private void dealList() {
        FocusListAdapter listAdapter = new FocusListAdapter(mActivity);
        focusListView.setAdapter(listAdapter);
        focusListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                FocusMsgBean focusMsgBean = (FocusMsgBean) adapterView.getAdapter().getItem(position);
                IntentUtils.into(getContext(), CWebViewActivity.class, focusMsgBean);
            }
        });
        /**
         * 获取数据
         */
        getListData(listAdapter);
    }

    private void getListData(FocusListAdapter listAdapter) {
        List arrayList = new ArrayList();
        arrayList.add(new FocusMsgBean("农普成果展示信息图", "http://www.life.uestc.edu.cn/static/img/upload/2017-10-311509411842.png",
                "/web/lib/module/mapResult/module" +
                        ".mapResult.html"));
        //http://121.42.160.106:8092/gallery2.1/mobile/index.html
        arrayList.add(new FocusMsgBean("图谱成果", "http://www.life.uestc.edu.cn/static/img/upload/2017-10-311509411842.png",
                "/gallery2.1/mobile/index.html"));
        listAdapter.setData(arrayList);
    }
}
