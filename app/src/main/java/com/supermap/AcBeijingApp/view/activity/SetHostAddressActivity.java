package com.supermap.AcBeijingApp.view.activity;

import android.text.TextUtils;

import com.supermap.AcBeijingApp.MainTabActivity;
import com.supermap.AcBeijingApp.R;
import com.supermap.AcBeijingApp.http.HttpUrl;
import com.supermap.AcBeijingApp.http.SharedPreferencesManager;
import com.supermap.AcBeijingApp.utils.IntentUtils;
import com.supermap.baselibrary.base.BaseActivity;
import com.supermap.baselibrary.base.IBasePresenter;
import com.supermap.baselibrary.widget.TagEditText;
import com.supermap.baselibrary.widget.TitleBar;

import butterknife.BindView;
import butterknife.OnClick;


/*
*  2017/9/13  -  16:14
*  设置服务器地址
*/
public class SetHostAddressActivity extends BaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.currentHost)
    TagEditText currentHost;
    @BindView(R.id.currentIp)
    TagEditText currentIp;
    @BindView(R.id.currentPort)
    TagEditText currentPort;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_set_host_address;
    }

    @Override
    protected void initCommon() {
        if (SharedPreferencesManager.getSetHost() && !TextUtils.isEmpty(SharedPreferencesManager.getUserIP())) {
            IntentUtils.into(this, MainTabActivity.class);
            finish();
        }
        titleBar.setTitle("设置服务器地址");
    }

    @Override
    protected void doBusiness() {
        String server = HttpUrl.getInstance().getServerHost();
        currentHost.setEditTextContent(server);
        currentIp.setEditTextContent(HttpUrl.getInstance().getSERVER_IP());
        currentPort.setEditTextContent(HttpUrl.getInstance().getSERVER_PORT());
    }

    @Override
    public IBasePresenter getPresenter() {
        return null;
    }

    @OnClick(R.id.set_ip_btn)
    public void onViewClicked() {
        setHost();
    }

    private void setHost() {
        String ip = currentIp.getEditTextContent();
        String port = currentPort.getEditTextContent();
        String server = ip + ":" + port + "/";
        if (TextUtils.isEmpty(ip) || TextUtils.isEmpty(port)) {
            showToast("请设置服务器地址");
            return;
        }
        //
        HttpUrl.getInstance().setSERVER_IP(ip);
        HttpUrl.getInstance().setSERVER_PORT(port);
        HttpUrl.getInstance().setServerHost(server);
        //
        SharedPreferencesManager.setUserIP(ip);
        SharedPreferencesManager.setUserPort(port);
        showToast("设置成功!");
        currentHost.setEditTextContent(HttpUrl.getInstance().getServerHost());
        //
        SharedPreferencesManager.setSetHost(true);
        IntentUtils.into(this, MainTabActivity.class);
        finish();
    }
}
