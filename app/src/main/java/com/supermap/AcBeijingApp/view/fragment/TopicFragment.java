package com.supermap.AcBeijingApp.view.fragment;


import android.view.View;
import android.widget.ExpandableListView;

import com.supermap.AcBeijingApp.R;
import com.supermap.AcBeijingApp.adapter.TopicListAdapter;
import com.supermap.AcBeijingApp.utils.SourceProvider;
import com.supermap.baselibrary.base.BaseFragment;
import com.supermap.baselibrary.widget.TitleBar;

import java.util.List;

import butterknife.BindView;

/**
 * 专题页面
 *
 * @author bruce
 *         2018/3/14 0014 下午 2:20
 */
public class TopicFragment extends BaseFragment {
    @BindView(R.id.topic_listView)
    ExpandableListView indexExpandListView;

    @BindView(R.id.titleBar)
    TitleBar titleBar;

    public TopicFragment() {
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_topic;
    }

    @Override
    protected void doBusiness(View rootView) {
        titleBar.setTitle("专题报告", false);
        List list = new SourceProvider().getIndexListData();
        TopicListAdapter adapter = new TopicListAdapter(getContext());
        indexExpandListView.setGroupIndicator(null);
        indexExpandListView.setAdapter(adapter);
        adapter.setData(list);
        indexExpandListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {

                return true;
            }
        });
        //遍历所有group,将所有项设置成默认展开
        int groupCount = indexExpandListView.getCount();
        for (int i = 0; i < groupCount; i++) {
            indexExpandListView.expandGroup(i);
        }
    }

}
