package com.supermap.AcBeijingApp.view.fragment;


import android.view.View;

import com.supermap.AcBeijingApp.R;
import com.supermap.AcBeijingApp.http.HttpUrl;
import com.supermap.baselibrary.base.BaseFragment;
import com.supermap.baselibrary.widget.MyWebView;
import com.supermap.baselibrary.widget.TitleBar;

import butterknife.BindView;

/**
 * 数据页面
 *
 * @author bruce
 *         2018/3/14 0014 下午 2:17
 */
public class DataFragment extends BaseFragment {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.webView)
    MyWebView webView;

    public DataFragment() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_data;
    }

    @Override
    protected void doBusiness(View rootView) {
        titleBar.setTitle("数据地图", false);
//        http://121.42.160.106:8092/web/lib/module/bulletinfolder/folder/one.html
//        String endUrl =  "/web/lib/module/bulletinfolder/folder/one.html";
        String endUrl =  "/visual/m.html";
        String url = HttpUrl.getInstance().getServerHost() +endUrl;
        webView.loadUrl(url);
    }

}
