package com.supermap.AcBeijingApp.view.fragment;


import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.supermap.AcBeijingApp.R;
import com.supermap.AcBeijingApp.view.activity.SetHostAddressActivity;
import com.supermap.baselibrary.base.BaseFragment;
import com.supermap.baselibrary.widget.TitleBar;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * 个人页面
 *
 * @author bruce
 *         2018/3/14 0014 下午 2:20
 */
public class PersonFragment extends BaseFragment {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.headView)
    CircleImageView headView;
    @BindView(R.id.userName)
    TextView userName;

    public PersonFragment() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_person;
    }

    @Override
    protected void doBusiness(View rootView) {
        titleBar.setTitle("我的", false);
    }

    @OnClick({R.id.head_layout, R.id.person_info, R.id.my_setting, R.id.about_us})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.head_layout:
                showToast("头像");
                break;
            case R.id.person_info:
                break;
            case R.id.my_setting:
                Intent intent = new Intent(getContext(), SetHostAddressActivity.class);
                startActivity(intent);
                break;
            case R.id.about_us:
                break;
        }
    }
}
