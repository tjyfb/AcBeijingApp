package com.supermap.AcBeijingApp.view.fragment;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.supermap.AcBeijingApp.R;
import com.supermap.AcBeijingApp.adapter.HomeListAdapter;
import com.supermap.AcBeijingApp.model.HomeNewsMsgBean;
import com.supermap.AcBeijingApp.utils.IntentUtils;
import com.supermap.AcBeijingApp.view.activity.CWebViewActivity;
import com.supermap.baselibrary.base.BaseFragment;
import com.supermap.baselibrary.widget.TitleBar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 首页
 *
 * @author bruce
 *         2018/3/14 0014 下午 2:18
 */
public class HomeFragment extends BaseFragment {
    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.listView)
    ListView listView;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void doBusiness(View rootView) {
        titleBar.setTitle("首页", false);
        dealList();
    }

    private void dealList() {
        HomeListAdapter listAdapter = new HomeListAdapter(mActivity);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                HomeNewsMsgBean homeNewsMsgBean = (HomeNewsMsgBean) adapterView.getAdapter().getItem(position);
                IntentUtils.into(getContext(), CWebViewActivity.class, homeNewsMsgBean);
            }
        });
        /**
         * 获取数据
         */
        getListData(listAdapter);
    }

    private void getListData(HomeListAdapter listAdapter) {
        List arrayList = new ArrayList();
        arrayList.add(new HomeNewsMsgBean("北京市第三次全国农业普查主要数据公报（第一号）", "/web/lib/module/bulletinfolder/folder/one.html"));
        arrayList.add(new HomeNewsMsgBean("北京市第三次全国农业普查主要数据公报（第二号）", "/web/lib/module/bulletinfolder/folder/two.html"));
        arrayList.add(new HomeNewsMsgBean("北京市第三次全国农业普查主要数据公报（第三号）", "/web/lib/module/bulletinfolder/folder/three.html"));
        arrayList.add(new HomeNewsMsgBean("北京市第三次全国农业普查主要数据公报（第四号）", "/web/lib/module/bulletinfolder/folder/four.html"));
        arrayList.add(new HomeNewsMsgBean("北京市第三次全国农业普查主要数据公报（第五号）", "/web/lib/module/bulletinfolder/folder/five.html"));
        arrayList.add(new HomeNewsMsgBean("北京市第三次全国农业普查主要数据公报（第六号）", "/web/lib/module/bulletinfolder/folder/six.html"));
        listAdapter.setData(arrayList);
    }
}
