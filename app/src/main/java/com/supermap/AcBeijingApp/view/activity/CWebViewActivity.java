package com.supermap.AcBeijingApp.view.activity;

import com.supermap.AcBeijingApp.R;
import com.supermap.AcBeijingApp.http.HttpUrl;
import com.supermap.AcBeijingApp.model.FocusMsgBean;
import com.supermap.AcBeijingApp.model.HomeNewsMsgBean;
import com.supermap.AcBeijingApp.utils.Constants;
import com.supermap.baselibrary.base.BaseActivity;
import com.supermap.baselibrary.base.IBasePresenter;
import com.supermap.baselibrary.widget.MyWebView;
import com.supermap.baselibrary.widget.TitleBar;

import butterknife.BindView;

/**
 * 公共的webView加载页面
 */
public class CWebViewActivity extends BaseActivity {

    @BindView(R.id.titleBar)
    TitleBar titleBar;
    @BindView(R.id.webView)
    MyWebView webView;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_cweb_view;
    }

    @Override
    protected void initCommon() {
        titleBar.setTitle("详情");
    }

    @Override
    protected void doBusiness() {
        Object object = getIntent().getSerializableExtra(Constants.INTENT_OBJECT);
        if (object instanceof HomeNewsMsgBean) {
            HomeNewsMsgBean homeNewsMsgBean = (HomeNewsMsgBean) object;
            String url = HttpUrl.getInstance().getServerHost() + homeNewsMsgBean.getUrl();
            webView.loadUrl(url);
        } else if (object instanceof FocusMsgBean) {
            FocusMsgBean focusMsgBean = (FocusMsgBean) object;
            String url = HttpUrl.getInstance().getServerHost() + focusMsgBean.getUrl();
            webView.loadUrl(url);
        }
    }

    @Override
    public IBasePresenter getPresenter() {
        return null;
    }

}
