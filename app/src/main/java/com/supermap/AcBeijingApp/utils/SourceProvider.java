package com.supermap.AcBeijingApp.utils;


import com.supermap.AcBeijingApp.model.IndexChildBean;
import com.supermap.AcBeijingApp.model.IndexParentBean;

import java.util.ArrayList;
import java.util.List;

/*
*  2017/9/21  -  14:15
*  数据提供工具类
*/
public class SourceProvider {
    public static SourceProvider sourceProvider = new SourceProvider();

    public SourceProvider() {

    }

    public static SourceProvider getInstance() {
        if (sourceProvider == null) {
            synchronized (SourceProvider.class) {
                sourceProvider = new SourceProvider();
            }
        }
        return sourceProvider;
    }

    /*
    *  2017/9/21  -  15:57
    *  获取检索数据列表
    */
    public List getIndexListData() {
        List list = new ArrayList();
        //
        List childList1 = new ArrayList();
        childList1.add(new IndexChildBean("乡镇", 10));
        childList1.add(new IndexChildBean("村", 11));
        childList1.add(new IndexChildBean("登记住户数量", 12));
        childList1.add(new IndexChildBean("登记人口数量", 13));
        IndexParentBean indexParentBean1 = new IndexParentBean("公报", childList1);
        //
        List childList2 = new ArrayList();
        childList2.add(new IndexChildBean("规模农业生产经营户和单位", 20));
        childList2.add(new IndexChildBean("农业从业人员", 21));
        childList2.add(new IndexChildBean("农业技术人员", 22));
        childList2.add(new IndexChildBean("主要农业机械数量", 23));
        childList2.add(new IndexChildBean("农业机械使用情况", 24));
        childList2.add(new IndexChildBean("设施农业", 25));
        IndexParentBean indexParentBean2 = new IndexParentBean("农民", childList2);
        //
        List childList3 = new ArrayList();
        childList3.add(new IndexChildBean("耕地分布及分类情况", 30));
        IndexParentBean indexParentBean3 = new IndexParentBean("农业", childList3);
        //
        List childList4 = new ArrayList();
        childList4.add(new IndexChildBean("交通", 40));
        childList4.add(new IndexChildBean("电力和通讯", 41));
        childList4.add(new IndexChildBean("文化教育", 42));
        childList4.add(new IndexChildBean("环境卫生", 43));
        childList4.add(new IndexChildBean("医疗和社会福利机构", 44));
        childList4.add(new IndexChildBean("市场建设", 45));
        IndexParentBean indexParentBean4 = new IndexParentBean("农村基础", childList4);
        //
        List childList5 = new ArrayList();
        childList5.add(new IndexChildBean("农村劳动力资源总量及构成", 50));
        childList5.add(new IndexChildBean("农村从业人员总量及构成", 51));
        childList5.add(new IndexChildBean("农村住户外出从业劳动力总量及构成", 52));
        childList5.add(new IndexChildBean("农村住户外出从业劳动力流向及就业情况", 53));
        IndexParentBean indexParentBean5 = new IndexParentBean("农村劳动力", childList5);
        //
        List childList6 = new ArrayList();
        childList6.add(new IndexChildBean("住宅", 60));
        childList6.add(new IndexChildBean("饮用水", 61));
        childList6.add(new IndexChildBean("卫生设施", 62));
        childList6.add(new IndexChildBean("炊事能源", 63));
        childList6.add(new IndexChildBean("耐用消费品", 64));
        IndexParentBean indexParentBean6 = new IndexParentBean("农村生活", childList6);
        //
        list.add(indexParentBean1);
        list.add(indexParentBean2);
        list.add(indexParentBean3);
        list.add(indexParentBean4);
        list.add(indexParentBean5);
        list.add(indexParentBean6);
        return list;
    }

}
