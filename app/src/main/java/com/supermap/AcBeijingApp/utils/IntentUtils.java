package com.supermap.AcBeijingApp.utils;

import android.content.Context;
import android.content.Intent;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/3/28 0028.
 */

public class IntentUtils {
    public static void into(Context context, Class c) {
        Intent intent = new Intent(context, c);
        context.startActivity(intent);
    }

    public static void into(Context context, Class c, String key, String value) {
        Intent intent = new Intent(context, c);
        intent.putExtra(key, value);
        context.startActivity(intent);
    }

    /*
    *  2017/10/13  -  10:04
    *  传递对象
    */
    public static void into(Context context, Class c, Serializable serializable) {
        Intent intent = new Intent(context, c);
        intent.putExtra(Constants.INTENT_OBJECT, serializable);
        context.startActivity(intent);
    }

    /*
  *  2017/10/13  -  10:04
  *  传递对象
  */
    public static void into(Context context, Class c, Serializable serializable, String value) {
        Intent intent = new Intent(context, c);
        intent.putExtra(Constants.INTENT_OBJECT, serializable);
        intent.putExtra("key", value);
        context.startActivity(intent);
    }
}
