package com.supermap.AcBeijingApp.utils;

import android.util.Log;

/**
 * Created by Administrator on 2017/3/23 0023.
 */

/**
 * 公用的log工具类
 */
public class Logs {
    private static boolean flag = true;
    private static String TAG = "bruce";

    public static void d(String msg) {
        if (flag) {
            Log.d(TAG, msg);
        }
    }

    public static void v(String msg) {
        if (flag) {
            Log.i(TAG, msg);
        }
    }

    public static void w(String msg) {
        if (flag) {
            Log.w(TAG, msg);
        }
    }

    public static void e(String msg) {
        if (flag) {
            Log.e(TAG, msg);
        }
    }
}
