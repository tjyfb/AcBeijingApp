package com.supermap.AcBeijingApp.model;

import java.io.Serializable;

/**
 * Created by Administrator on 2017/5/19 0019.
 */
/*
*  2017/9/12  -  15:22
*  首页新闻列表bean
*/
public class HomeNewsMsgBean implements Serializable {
    private String showMsg;
    private String url;

    public HomeNewsMsgBean() {

    }

    public HomeNewsMsgBean(String showMsg, String url) {
        this.showMsg = showMsg;
        this.url = url;
    }

    public String getShowMsg() {
        return showMsg;
    }

    public void setShowMsg(String showMsg) {
        this.showMsg = showMsg;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
