package com.supermap.AcBeijingApp.model;

import java.io.Serializable;

/**
 * 客户关注list
 *
 * @author bruce
 *         2018/3/14 0014 下午 4:42
 */
public class FocusMsgBean implements Serializable {
    private String title;
    private String imgUrl;
    private String url;

    public FocusMsgBean() {

    }

    public FocusMsgBean(String title, String imgUrl) {
        this.title = title;
        this.imgUrl = imgUrl;
    }

    public FocusMsgBean(String title, String imgUrl, String url) {
        this.title = title;
        this.imgUrl = imgUrl;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
