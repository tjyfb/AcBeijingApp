package com.supermap.AcBeijingApp.model;

import java.util.List;

/**
 * Created by bruce on 2017/10/25 -- 17:44
 */

public class IndexParentBean {
    private String content;
    private List<IndexChildBean> list;

    public IndexParentBean(String content, List<IndexChildBean> list) {
        this.content = content;
        this.list = list;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<IndexChildBean> getList() {
        return list;
    }

    public void setList(List<IndexChildBean> list) {
        this.list = list;
    }
}
