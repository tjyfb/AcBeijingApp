package com.supermap.AcBeijingApp.model;

/**
 * Created by bruce on 2017/10/25 -- 17:45
 */

public class IndexChildBean {
    private String content;
    private int beanId;

    public IndexChildBean(String content, int beanId) {
        this.content = content;
        this.beanId = beanId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getBeanId() {
        return beanId;
    }

    public void setBeanId(int beanId) {
        this.beanId = beanId;
    }
}
